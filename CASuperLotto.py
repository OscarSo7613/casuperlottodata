#!/usr/bin/env python3

#
# Visit: https://oscarsoft.co/cgi-bin/CASuperLotto.py
#
from datetime import datetime

import base64
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cgitb


pd.set_option('display.max_columns', 10)
pd.set_option('display.width', 1000)

cgitb.enable()
print("Content-Type: text/plain;charset=utf-8\n")

print("\n\nCalifornia SuperLotto Drawing Prediction\n\n")

ts = 'Time Now: ' + str(datetime.now()) + '\n'
print(ts)

fn = 'casuperlotto-asc-short.csv'
#pddp = pd.read_csv(fn)
pddp = pd.read_csv(
                    fn,
                    header=0,
                    names=["DrawNum", "DrawDate", "d1", "d2", "d3", "d4", "d5", "Mega"]
                    )

print("<---------- read_csv ---------->")
print(pddp)
print("\n\n")

print("<---------- head ---------->")
print(pddp.head())
print("\n\n")

print("<---------- describe ---------->")
s = pddp[["DrawNum","d1","d2","d3","d4","d5","Mega"]].describe()
print(s)
print("\n\n")


print("<---------- s2_num init ---------->")
MAX_NUM = 47
s2_num =  {}
i_num = 1
while True:
    j_num = 1
    while True:
        str_index = str(i_num) + ',' + str(j_num)
        s2_num[str_index] = 0
        j_num = j_num + 1
        if ( j_num > MAX_NUM ):
            break
    i_num = i_num + 1
    if ( i_num > MAX_NUM ):
        break

print("<---------- s2_mega init ---------->")
MAX_MEGA = 27
s2_mega =  {}
i_mega = 1
while True:
    j_mega = 1
    while True:
        str_index = str(i_mega) + ',' + str(j_mega)
        s2_mega[str_index] = 0
        j_mega = j_mega + 1
        if ( j_mega > MAX_MEGA ):
            break
    i_mega = i_mega + 1
    if ( i_mega > MAX_MEGA ):
        break

print("<---------- iter pd and count for s2_num  ---------->")
pd_prev = pddp.iloc[0]
for index, pd_cur in pddp.iterrows():
    if index >= 1:
        # Permute d1 - d5
        # d1, d1
        key = str(pd_prev['d1']) + "," + str(pd_cur['d1'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        # d1, d2
        key = str(pd_prev['d1']) + "," + str(pd_cur['d2'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        # d1, d3
        key = str(pd_prev['d1']) + "," + str(pd_cur['d3'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        # d1, d4
        key = str(pd_prev['d1']) + "," + str(pd_cur['d4'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        # d1, d5
        key = str(pd_prev['d1']) + "," + str(pd_cur['d5'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))

        #----- d2, d1
        key = str(pd_prev['d2']) + "," + str(pd_cur['d1'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #----- d2, d2
        key = str(pd_prev['d2']) + "," + str(pd_cur['d2'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #----- d2, d3
        key = str(pd_prev['d2']) + "," + str(pd_cur['d3'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #----- d2, d4
        key = str(pd_prev['d2']) + "," + str(pd_cur['d4'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #----- d2, d5
        key = str(pd_prev['d2']) + "," + str(pd_cur['d5'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))

        #-----  d3, d1
        key = str(pd_prev['d3']) + "," + str(pd_cur['d1'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d3, d2
        key = str(pd_prev['d3']) + "," + str(pd_cur['d2'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d3, d3
        key = str(pd_prev['d3']) + "," + str(pd_cur['d3'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d3, d4
        key = str(pd_prev['d3']) + "," + str(pd_cur['d4'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d3, d5
        key = str(pd_prev['d3']) + "," + str(pd_cur['d5'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))

        #-----  d4, d1
        key = str(pd_prev['d4']) + "," + str(pd_cur['d1'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d4, d2
        key = str(pd_prev['d4']) + "," + str(pd_cur['d2'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d4, d3
        key = str(pd_prev['d4']) + "," + str(pd_cur['d3'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d4, d4
        key = str(pd_prev['d4']) + "," + str(pd_cur['d4'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d4, d5
        key = str(pd_prev['d4']) + "," + str(pd_cur['d5'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))

        #-----  d5, d1
        key = str(pd_prev['d5']) + "," + str(pd_cur['d1'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d5, d2
        key = str(pd_prev['d5']) + "," + str(pd_cur['d2'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d5, d3
        key = str(pd_prev['d5']) + "," + str(pd_cur['d3'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d5, d4
        key = str(pd_prev['d5']) + "," + str(pd_cur['d4'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))
        #-----  d5, d5
        key = str(pd_prev['d5']) + "," + str(pd_cur['d5'])
        cnt_old = s2_num[key]
        s2_num[key] = cnt_old + 1
        print("s2_num[" + key + "] = " + str(s2_num[key]))

        # -------------------------------------------------------- Permute MEGA
        key = str(pd_prev['Mega']) + "," + str(pd_cur['Mega'])
        print("Mega key=" + key)
        cnt_old = int(s2_mega[key])
        s2_mega[key] = cnt_old + 1
        print("s2_mega[" + key + "] = " + str(s2_mega[key]))

        #set current draw to previous draw
        pd_prev = pd_cur


print("<---------- Data Model ---------->")
print("<---------- s2_num ---------->")
i_num = 1
while True:
    j_num = 1
    while True:
        key = str(i_num) + ',' + str(j_num)
        value = int(s2_num[key])
        if value > 0:
            print("DM: s2_num[" + key + "] = " + str(value))
        j_num = j_num + 1
        if ( j_num > MAX_NUM ):
            break
    i_num = i_num + 1
    if ( i_num > MAX_NUM ):
        break

print("<---------- s2_mega ---------->")
i_mega = 1
while True:
    j_mega = 1
    while True:
        key = str(i_mega) + ',' + str(j_mega)
        value = int(s2_mega[key])
        if value > 0:
            print("DM: s2_mega[" + key + "] = " + str(value))
        j_mega = j_mega + 1
        if ( j_mega > MAX_MEGA ):
            break
    i_mega = i_mega + 1
    if ( i_mega > MAX_MEGA ):
        break

