#!/usr/bin/python

import sys, getopt

# Steps:
# 1) Visit https://www.calottery.com/draw-games/superlotto-plus#section-content-2-3
# 2) Use mouse to highlight the whole area of drawn numbers
# 3) Paste into a text file called it yyyymmdd_draw.txt
# 4) Run: python lottowebdataparser_a.py -i yyyymmdd_draw.txt yyyymmdd_draw.txt.out
#
def setTup(cnt,line,tup):
   if cnt == 1:
      date = line;
      tup["date"] = "Wed. " + date.rstrip() 
   if cnt == 4:
      draw = line;
      tup["draw"] = draw.rstrip().replace('#','')
   if cnt == 12:
      win = line;
      win = win.replace('Winning Numbers, ','')
      win = win.replace('Mega, #', '')
      win = win.replace(' ', ';')
      win = win.rstrip()
      win = win + ";"
      tup["win"] = win
   return tup


def parse(inf,outf):
   with open(inf) as fpr:
      line = fpr.readline()
      cnt = 1
      with open(outf, 'w') as fpw:
         tup = {"date": "", "draw": "", "win": ""} 
         while line:
            tup = setTup(cnt,line,tup)
            if cnt == 12:
               #fpw.write("{};{};{}\n".format(tup["draw"],tup["date"],tup["win"]))
               tup = {}
            line = fpr.readline()
            cnt += 1
            if cnt % 16 == 0: cnt = 1 
   fpr.close()
   fpw.close()


def main(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["in=","out="])
   except getopt.GetoptError:
      print 'test.py -i <in> -o <out>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'test.py -i <in> -o <out>'
         sys.exit()
      elif opt in ("-i", "--in"):
         inputfile = arg
      elif opt in ("-o", "--out"):
         outputfile = arg
   parse(inputfile,outputfile)

if __name__ == "__main__":
   main(sys.argv[1:])
