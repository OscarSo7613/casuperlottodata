#!/usr/bin/python

import sys, getopt


#
# Sample:
# python lottowebdataparser_b.py -d 3104 -i "data/2016_lottery_result.xml" -o 2016.txt
#

def setTup(cnt,line,tup):
   if cnt == 3:
      date = line;
      date = date.rstrip()
      date = date.replace('</a>','')
      m8ddyyyy = date.rpartition("<br>")
      date = m8ddyyyy[2].partition(' ')
      m8 = date[0]
      ddyyyy = date[2]
      date = ddyyyy.partition(', ')
      dd = int(date[0])
      print("dd: " + str(dd))
      if (dd < 10):
         day = "0" + str(dd)
      else:
         day = str(dd)
      print("day: " + day)
      yyyy = date[2]
      tup['date'] = "Wed. " + m8[0:3] + " " + day + ", " + yyyy 
   if cnt == 9:
      d1 = line
      d1 = d1.rstrip()
      d1 = d1.replace('\t', '')
      d1 = d1.replace(' ', '')
      tup['d1'] = d1
   if cnt == 13:
      d2 = line
      tup['d2'] = d2.rstrip().replace('\t', '')
   if cnt == 17:
      d3 = line
      tup['d3'] = d3.rstrip().replace('\t', '')
   if cnt == 21:
      d4 = line
      tup['d4'] = d4.rstrip().replace('\t', '')
   if cnt == 25:
      d5 = line
      tup['d5'] = d5.rstrip().replace('\t', '')
   if cnt == 29:
      mega = line
      tup['mega'] = mega.rstrip().replace('\t', '')
   return tup


def parse(dn,inf,outf):
   dn = int(dn)
   with open(inf) as fpr:
      line = fpr.readline()
      cnt = 1
      with open(outf, 'w') as fpw:
         tup = { 
                'date': '',
                'draw': '',
                  'd1': '',
                  'd2': '',
                  'd3': '',
                  'd4': '',
                  'd5': '',
                'mega': ''
               } 
         while line:
            tup = setTup(cnt,line,tup)
            if cnt == 29:
               fpw.write("{};{};{};{};{};{};{};{};\n".format(dn, tup["date"],
                                             tup["d1"], tup["d2"], tup["d3"],
                                             tup["d4"], tup["d5"], tup["mega"]))
               dn = dn - 1
            line = fpr.readline()
            cnt += 1
            if cnt % 35 == 0: cnt = 1
   fpr.close()
   fpw.close()


def main(argv):
   dn = 0
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"h:d:i:o:",["dn=","in=","out="])
   except getopt.GetoptError:
      print 'test.py -d <dn> -i <in> -o <out>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'test.py -d <dn> -i <in> -o <out>'
         sys.exit()
      elif opt in ("-d", "--dn"):
         dn = arg
      elif opt in ("-i", "--in"):
         infile = arg
      elif opt in ("-o", "--out"):
         outfile = arg
   parse(dn,infile,outfile)

if __name__ == "__main__":
   main(sys.argv[1:])
